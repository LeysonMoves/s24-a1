// NUMBER 2- OR OPERATOR
db.users.find({ 
	$or: [
		{
		"firstName":"Jane" 
		}, 
		{
		"firstName":"Stephen"
		
		]},

		{
		"_id": 0,
		"firstName":1,
		"lastName":1
		}

);

/*
db.users.find(
		{ 
		firstName:{
			$ne: "Jane"
					}
		}, 
		
		{
		firstName:1,
		lastName:1
		}
);


*/

// NUMBER 3 - AND OEPRATOR
db.users.find({ 
	$and: [
		{
		"department":"HR" 
		}, 
		{
		"age":{$gte:70} 
		}
	] 
});


// NUMBER 44 -SAND, REGEX AND LTE OPERATORS
db.users.find({ 
	$and: [
		{
		"firstName": {$regex: "N", $options: "$i"}
		}, 
		{
		"age":{$lte:30} 
		}
	] 
});

// DONE
